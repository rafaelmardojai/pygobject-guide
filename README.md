# PyGObject Guide

Welcome to the source of the [PyGObject Guide](https://rafaelmardojai.pages.gitlab.gnome.org/pygobject-guide/).

## How to edit the documentation

You can add, remove, and modify the content by editing the `.rst` files under `source`, using your preferred text editor.

## How to build the documentation locally

You will need Python 3.

You will need to install Sphinx and the Furo theme:

```
pip install --user --upgrade sphinx sphinx-copybutton furo
```

To build the site you can run:

```
./build.sh
```

from the project's root. The build output will be in the `build` directory.

Alternativelly you can install sphinx-autobuild to have a live site while editing:

```
pip install --user --upgrade sphinx-autobuild
```

To start the live site run:

```
sphinx-autobuild source build
```

## Credits

Most parts of this guide are a fork of the [PyGObject-Tutorial](https://github.com/sebp/PyGObject-Tutorial) by [Sebastian Pölsterl](https://k-d-w.org/) with an updated structure and ported to GTK4, initially made by [Rafael Mardojai CM](https://mardojai.com).
