Acknowledgements
================

Most parts of this guide are a fork of the `PyGObject-Tutorial <https://github.com/sebp/PyGObject-Tutorial>`_
by `Sebastian Pölsterl (sebp) <https://k-d-w.org/>`_ with an updated structure
and ported to GTK4, initially made by `Rafael Mardojai CM <https://mardojai.com>`_.

It also takes some inspiration from `<https://gjs.guide/>`_.

----

The source files of this guide can be found
`here <https://gitlab.gnome.org/rafaelmardojai/pygobject-guide>`_.
