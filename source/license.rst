License
-------

The contents of this guide are licensed under the Lesser GNU Public License
2.1.

.. literalinclude:: ../COPYING
    :caption: Full License Text
    :language: text
    :linenos:
